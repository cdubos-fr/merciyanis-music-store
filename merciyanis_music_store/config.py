from decouple import config


class AppConfig:
    csrf_secret: str = config("CSRF_SECRET")
    db_name: str = config("DB_NAME")
    auth_secret: str = config("AUTH_SECRET")


def get_config() -> AppConfig:
    return AppConfig()
