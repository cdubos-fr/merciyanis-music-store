from fastapi import FastAPI
from fastapi import Request
from fastapi import Response
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi_csrf_protect import CsrfProtect
from fastapi_csrf_protect.exceptions import CsrfProtectError
from pydantic import BaseModel

from merciyanis_music_store.config import get_config
from merciyanis_music_store.routers import albums
from merciyanis_music_store.routers import auth
from merciyanis_music_store.routers import cart
from merciyanis_music_store.routers import index


app = FastAPI()

# AUTH middleware
auth.manager.useRequest(app)


# CSRF
class CsrfSettings(BaseModel):
    secret_key: str = get_config().csrf_secret
    cookie_samesite: str = "none"
    cookie_secure: bool = True
    token_location: str = "body"
    token_key: str = "csrf-token"


@CsrfProtect.load_config
def get_csrf_config() -> CsrfSettings:
    return CsrfSettings()


@app.exception_handler(CsrfProtectError)
def csrf_protect_exception_handler(request: Request, exc: CsrfProtectError) -> Response:
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.message})


# STATIC
app.mount(
    "/static",
    StaticFiles(directory="merciyanis_music_store/static"),
    name="static",
)


app.include_router(index.router)
app.include_router(albums.router)
app.include_router(auth.router)
app.include_router(cart.router)
