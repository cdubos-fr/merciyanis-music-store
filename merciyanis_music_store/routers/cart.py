from typing import Any

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Request
from fastapi import Response
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from sqlmodel import col
from sqlmodel import select

from merciyanis_music_store.db.conn import db_session
from merciyanis_music_store.db.models import Album
from merciyanis_music_store.db.models import Cart
from merciyanis_music_store.routers import templates
from merciyanis_music_store.routers.auth import manager

router = APIRouter()


class AlbumId(BaseModel):
    album_id: int


@router.post("/cart/add")
async def add_to_cart(album: AlbumId, user: Any = Depends(manager)) -> JSONResponse:
    with db_session() as session:
        cart = session.exec(
            select(Cart).where(Cart.user_id == user.id),
        ).first() or Cart(albums=[], user_id=user.id)
        cart.albums = cart.albums + [album.album_id]
        session.add(cart)
        session.commit()
    return JSONResponse({"status": "success"})


@router.get("/cart")
def purchase(request: Request, user: Any = Depends(manager)) -> Response:
    with db_session() as session:
        a = session.exec(select(Cart.albums).where(Cart.user_id == user.id)).first()
        albums_cart = session.exec(
            select(Album).where(
                col(Album.id).in_(a),
            ),
        ).all()
    return templates.TemplateResponse(
        "purchase.html",
        context={
            "request": request,
            "user": user,
            "albums": albums_cart,
            "total_price": sum(album.price for album in albums_cart),
        },
    )


@router.post("/cart/purchase")
def finalize_purchase(request: Request, user: Any = Depends(manager)) -> Response:
    with db_session() as session:
        cart = session.exec(
            select(Cart).where(Cart.user_id == user.id),
        ).first() or Cart(user_id=user.id, albums=[])
        cart.albums = []
        session.add(cart)
        session.commit()
    return templates.TemplateResponse("thanks.html", context={"request": request, "user": user})
