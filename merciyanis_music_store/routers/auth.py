from typing import cast

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Request
from fastapi import Response
from fastapi.responses import RedirectResponse
from fastapi_csrf_protect import CsrfProtect
from fastapi_login import LoginManager
from sqlmodel import exists
from sqlmodel import select

from merciyanis_music_store.config import get_config
from merciyanis_music_store.db.conn import db_session
from merciyanis_music_store.db.models import user
from merciyanis_music_store.routers import templates

router = APIRouter()


COOKIE_AUTH_NAME = "access-token"

manager = LoginManager(
    get_config().auth_secret,
    '/auth/token',
    use_cookie=True,
    cookie_name=COOKIE_AUTH_NAME,
    use_header=False,
)


@manager.user_loader()
def query_user(email: str) -> user.User | None:
    with db_session() as session:
        db_user = session.exec(select(user.User).where(user.User.email == email)).first()
    return db_user


def create_auth_page(
        template: str,
        request: Request,
        csrf_protect: CsrfProtect,
        errors: str = "",
) -> Response:
    csrf_token, signed_token = csrf_protect.generate_csrf_tokens()
    response = templates.TemplateResponse(
        template, {"request": request, "csrf_token": csrf_token, "errors": errors},
    )
    csrf_protect.set_csrf_cookie(signed_token, response)
    return response


@router.get("/register")
def register_form(request: Request, csrf_protect: CsrfProtect = Depends()) -> Response:
    return create_auth_page("register.html", request, csrf_protect)


@router.post('/register', tags=["user"])
async def register(
    request: Request,
    csrf_protect: CsrfProtect = Depends(),
) -> Response:
    await csrf_protect.validate_csrf(request)

    form_data = await request.form()
    db_user = user.User(email=form_data.get("email"), password=form_data.get("password"))
    db_user.hash_password()

    with db_session() as session:
        user_already_exist = session.exec(
            select(  # type: ignore[call-overload]
                exists(
                    user.User,
                ).where(
                    user.User.email == db_user.email,
                ),
            ),
        ).first()
        if user_already_exist:
            return create_auth_page(
                "register.html",
                request,
                csrf_protect,
                errors="email already used",
            )
        session.add(db_user)
        session.commit()
    return RedirectResponse(
        request.url_for("root"), status_code=301,
    )


@router.get("/login")
def form(request: Request, csrf_protect: CsrfProtect = Depends()) -> Response:
    return create_auth_page("login.html", request, csrf_protect)


@router.post('/login')
async def login(
    request: Request,
    csrf_protect: CsrfProtect = Depends(),
) -> Response:
    await csrf_protect.validate_csrf(request)

    form_data = await request.form()
    email = cast(str, form_data.get("email"))
    password = cast(str, form_data.get("password"))

    db_user = user.User.get_user(email)

    response: Response
    if db_user and db_user.is_valid_user(password):
        response = RedirectResponse(request.url_for("root"), status_code=301)
        access_token = manager.create_access_token(
            data={'sub': email},
        )
        manager.set_cookie(response, access_token)
    else:
        response = create_auth_page(
            "login.html", request, csrf_protect,
            errors="Invalid email or password",
        )

    csrf_protect.unset_csrf_cookie(response)
    return response


@router.get('/logout')
def logout(response: Response) -> Response:
    response = RedirectResponse("/", status_code=302)
    response.set_cookie(COOKIE_AUTH_NAME, expires=0, max_age=0, secure=True, samesite='none')
    return response
