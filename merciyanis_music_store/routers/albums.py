import math
from typing import Any

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Request
from fastapi import Response
from sqlmodel import func
from sqlmodel import select

from merciyanis_music_store.db.conn import db_session
from merciyanis_music_store.db.models import album
from merciyanis_music_store.routers import auth
from merciyanis_music_store.routers import templates

router = APIRouter()


@router.get("/albums")
def albums(request: Request, page: int = 0, user: Any = Depends(auth.manager.optional)) -> Response:

    with db_session() as session:
        counted_album = session.exec(
            select(func.count(album.Album.id)),  # type: ignore[call-overload]
        ).first()
        albums_collection = session.exec(select(album.Album).limit(51).offset(51 * page)).all()
    return templates.TemplateResponse(
        "albums.html",
        context={
            "request": request,
            "albums": albums_collection,
            "user": user,
            "page": page,
            "max_page": math.ceil(
                (counted_album or 0) / 51,
            ),
        },
    )
