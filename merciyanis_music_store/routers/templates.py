from fastapi.templating import Jinja2Templates


templates = Jinja2Templates(
    directory="merciyanis_music_store/templates",
)
