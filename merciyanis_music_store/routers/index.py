from typing import Any

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Request
from fastapi import Response

from merciyanis_music_store.routers import auth
from merciyanis_music_store.routers import templates

router = APIRouter()


@router.get("/")
def root(request: Request, user: Any = Depends(auth.manager.optional)) -> Response:
    print("is here ?")
    return templates.TemplateResponse(
        "index.html", context={"request": request, "user": user},
    )
