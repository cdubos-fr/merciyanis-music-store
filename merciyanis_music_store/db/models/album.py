from typing import TypedDict

from faker import Faker
from polyfactory import Ignore
from polyfactory.factories import TypedDictFactory
from polyfactory.factories.pydantic_factory import ModelFactory
from sqlmodel import Column
from sqlmodel import Field
from sqlmodel import JSON
from sqlmodel import SQLModel


class SongWithDuration(TypedDict):
    name: str
    duration: int
    artists: list[str]


class Album(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str
    price: int
    cover: str
    description: str
    artists: list[str] = Field(sa_column=Column(JSON))
    songs_with_duration: list[SongWithDuration] = Field(sa_column=Column(JSON))
    categories: list[str] = Field(sa_column=Column(JSON))
    label: str


class SongsFactory(TypedDictFactory[SongWithDuration]):
    __model__ = SongWithDuration
    __faker__ = Faker(locale="fr_FR")

    @classmethod
    def name(cls) -> str:
        return cls.__faker__.sentence(5)

    @classmethod
    def duration(cls) -> int:
        return cls.__faker__.random.randint(120, 600)  # nosec

    @classmethod
    def artists(cls) -> list[str]:
        import random
        return [cls.__faker__.name() for i in range(random.randint(1, 2))]  # nosec


class AlbumFactory(ModelFactory[Album]):
    __faker__ = Faker(locale="fr_FR")
    __model__ = Album
    id = Ignore()

    @classmethod
    def songs_with_duration(cls) -> list[SongWithDuration]:
        import random
        return SongsFactory.batch(size=random.randint(5, 10))  # nosec

    @classmethod
    def name(cls) -> str:
        return cls.__faker__.sentence(3)

    @classmethod
    def price(cls) -> int:
        return cls.__faker__.random.randint(1, 35)  # nosec

    @classmethod
    def cover(cls) -> str:
        return cls.__faker__.random.choice([f"default-{i}.jpg" for i in range(1, 5)])  # nosec

    @classmethod
    def description(cls) -> str:
        return "\n".join(cls.__faker__.sentences(6))

    @classmethod
    def artists(cls) -> list[str]:
        import random
        return [cls.__faker__.name() for i in range(random.randint(1, 3))]  # nosec

    @classmethod
    def label(cls) -> str:
        return cls.__faker__.company()

    @classmethod
    def categories(cls) -> list[str]:
        return cls.__faker__.random.choices([  # nosec
            "Acid blues",
            "Acid breaks",
            "Acid house",
            "Acid jazz",
            "Acid rock",
            "Acid techno",
            "Acid trance",
            "Acidcore",
            "Adult contemporary",
            "Afrobeat",
            "Afropop",
            "Afrotrap",
            "Aguinaldo (en)",
            "Allaoui",
            "Amapiano",
            "Ambient",
            "Ambient house",
            "Ambient jungle",
            "Blues rock",
            "Blues touareg",
            "Blues traditionnel",
            "Boléro",
            "Bongo Flava",
            "Boogaloo",
            "Boogie-woogie",
            "Bossa nova",
            "Bounce music",
            "Brass band",
            "Breakbeat",
            "Breakbeat hardcore",
            "Breakcore",
            "Britpop",
            "Broken beat",
            "Brutal death metal",
            "Bubblegum pop",
            "Bubblegum dance",
            "C86",
            "C-pop",
            "Calypso",
            "Cantopop",
            "Cello rock",
            "Cha-cha-cha",
            "Chaâbi algérien",
            "Chaâbi marocain",
            "Changüí",
            "Chanson française",
            "Chant grégorien",
            "Chicago blues",
            "Chicago house",
            "Chill-out",
            "Chillstep",
            "Death metal mélodique",
            "Death metal technique",
            "Death 'n' roll",
            "Death rock",
            "Deathcore",
            "Deathcountry",
            "Deathgrind",
            "Deep house",
            "Deepkho",
            "Delta blues",
            "Detroit blues",
            "Digital hardcore",
            "Dirty South",
            "Disco",
            "Disco house",
            "Disco polo",
            "Diva house",
            "Dixieland",
            "Djent",
            "Ethno-jazz",
            "Euro disco",
            "Eurobeat",
            "Eurodance",
            "Europop",
            "Extratone",
            "Folk rock",
            "Folktronica",
            "Freakbeat",
            "Free jazz",
            "Freeform hardcore",
            "Freestyle",
            "French touch",
            "Frenchcore",
            "Fun-punk",
            "Hardtechno",
            "Heartland rock",
            "Heavy metal",
            "Heavy metal traditionnel",
            "Hi-NRG",
            "Highlife",
            "Hip-hop",
            "Hip-hop alternatif",
            "Hip-hop australien",
            "Hip-hop chrétien",
            "Hip-hop expérimental",
            "Hip-hop old-school",
            "Hip-hop orchestral",
            "Indie pop",
            "Industrial hardcore",
            "Intelligent dance music (IDM)",
            "Jazz vocal",
            "Jazz West Coast",
            "Jazzstep",
            "Jump blues",
            "Jump-up",
            "Jumpstyle",
            "Jungle",
            "K-pop",
            "Kaneka",
            "Kansas City blues",
            "Kasékò",
            "Medieval rock",
            "Memphis blues",
            "Merengue",
            "Merenhouse",
            "Metal alternatif",
            "Metal avant-gardiste",
            "Metal celtique",
            "Neue Deutsche Härte",
            "Neue Deutsche Welle",
            "Neurofunk",
            "New age",
            "New beat",
            "New jack swing",
            "New prog",
            "New wave",
            "New wave of American heavy metal (NWOAHM)",
            "New wave of British heavy metal (NWOBHM)",
            "New York blues",
            "New York hardcore",
            "Opéra",
            "Opéra-rock",
            "Power pop",
            "Powerviolence",
            "Prélude",
            "Progressive psytrance",
            "Protopunk",
            "Psychobilly",
            "Pub rock",
            "Punk blues",
            "Punk celtique",
            "Punk chrétien",
            "Punk folk",
            "Punk hardcore (ou hardcore)",
            "Punk rock",
            "Punta rock",
            "Punto guajiro",
            "Queercore",
            "Quiet storm",
            "Rabiz",
            "Raga rock",
            "Ragga",
            "Ragtime",
            "Raï",
            "Raï'n'B",
            "Rap",
            "Rap East Coast",
            "Rap hardcore",
            "Rap metal",
            "Rap politique",
            "Rap rock",
            "Rap West Coast",
            "Rapcore",
            "Rave",
            "Soul progressive",
            "Soul psychédélique",
            "Southern gospel",
            "Southern soul",
            "Space rock",
            "Speed garage",
            "Speed metal",
            "Speedcore",
            "Splittercore",
            "Stoner rock",
            "Street punk",
            "Sunshine pop",
            "Surf",
            "Swamp blues",
            "Swamp pop",
            "Swing",
            "Symphonie",
            "Synthpop (ou electropop ou technopop)",
            "Synthpunk (ou electropunk)",
            "Synthwave",
            "Trance psychédélique",
            "Trance vocale",
            "Trap",
            "Tribal house",
            "Tribe",
            "Trip hop",
            "Tropical house",
            "Tropipop",
            "Tumba francesa",
            "Twarab (ou taarab)",
            "Twee pop",
            "UK garage",
            "UK hardcore",
            "Visual kei",
            "Vocal house",
            "World music",
            "Yéyé",
            "Ziglibithy",
            "Zouglou",
            "2-step garage",
        ])


def add_data() -> None:
    from merciyanis_music_store.db.conn import db_session
    with db_session() as session:
        for _ in range(10000):
            session.add(AlbumFactory.build())
        session.commit()
