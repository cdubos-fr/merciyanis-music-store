from typing import Optional
from typing import TYPE_CHECKING

from passlib.hash import pbkdf2_sha256
from pydantic import EmailStr
from sqlmodel import Field
from sqlmodel import Relationship
from sqlmodel import select
from sqlmodel import SQLModel

from merciyanis_music_store.db.conn import db_session

if TYPE_CHECKING:
    from .cart import Cart


class User(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    email: EmailStr
    password: str

    cart: Optional["Cart"] = Relationship(
        back_populates="user",
        sa_relationship_kwargs={
            "uselist": False,
        },
    )

    @classmethod
    def hash_function(cls, password: str) -> str:
        return pbkdf2_sha256.hash(password)

    @classmethod
    def get_user(cls, email: str) -> Optional["User"]:
        with db_session() as session:
            statement = select(User).where(
                cls.email == email,
            )
            return session.exec(statement).first()

    def is_valid_user(self, password: str) -> bool:
        return pbkdf2_sha256.verify(password, self.password)

    def hash_password(self) -> None:
        self.password = self.hash_function(self.password)
