from typing import TYPE_CHECKING

from sqlmodel import Column
from sqlmodel import Field
from sqlmodel import JSON
from sqlmodel import Relationship
from sqlmodel import SQLModel

if TYPE_CHECKING:
    from .user import User


class Cart(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    user_id: int = Field(foreign_key="user.id")
    albums: list[int] = Field(sa_column=Column(JSON))
    user: "User" = Relationship(back_populates="cart", sa_relationship_kwargs={"uselist": False})
