from collections.abc import Generator
from contextlib import contextmanager

from sqlalchemy.future.engine import Engine
from sqlmodel import create_engine
from sqlmodel import Session

from merciyanis_music_store.config import get_config


def get_engine() -> Engine:
    return create_engine(
        f'sqlite:///{get_config().db_name}',
        connect_args={
            'check_same_thread': False,
        },
    )


@contextmanager
def db_session() -> Generator[Session, None, None]:
    yield Session(get_engine())
