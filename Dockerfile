FROM python:3.11


ADD . /code
WORKDIR /code

ENV DB_NAME=database.db
RUN /bin/bash -l -c 'echo AUTH_SECRET="$(openssl rand -hex 64)" >> .env'
RUN /bin/bash -l -c 'echo CSRF_SECRET="$(openssl rand -hex 64)" >> .env'
RUN pip install --no-cache-dir .["db-migration"]
RUN alembic upgrade head

RUN python -c "from merciyanis_music_store.db.models.album import add_data; add_data()"

CMD ["uvicorn", "merciyanis_music_store.app:app", "--host", "0.0.0.0", "--port", "8000"]
