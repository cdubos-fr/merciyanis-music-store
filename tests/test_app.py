import os
from collections.abc import Generator

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.engine import Engine
from sqlmodel import SQLModel

from merciyanis_music_store.app import app
from merciyanis_music_store.db.conn import get_engine


@pytest.fixture
def client():
    return TestClient(app)


@pytest.fixture(scope="session")
def db_name() -> Generator[str, None, None]:
    os.environ["DB_NAME"] = "test.db"
    yield os.environ["DB_NAME"]


@pytest.fixture(scope="session")
def alembic_engine(db_name: str) -> Generator[Engine, None, None]:
    engine = get_engine()
    SQLModel.metadata.create_all(bind=engine)
    yield engine
    os.remove(db_name)


def test_welcome_page(client: TestClient) -> None:
    response = client.get('/')
    assert response.status_code == 200
    assert "Welcome on MerciYanis" in response.text


def test_create_user(alembic_engine: Engine, client: TestClient) -> None:
    email = "an_email@gmail.com"
    response = client.post(
        "/register", data={
            "email": email,
            "password": "a_password",
        },
    )
    assert response.status_code == 400
