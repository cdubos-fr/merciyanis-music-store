MerciYanis Music Store Webpage

Designed mention:
- [Webpixels](https://webpixels.io/?ref=appseed)
- coded by: [AppSeed](https://appseed.us/)

# Developpement
## Installation

```bash
$ pip install <path-to-this-project>
```
or
```bash
$ pip install git+<git-url>
```

to install it in editable mode:
```bash
$ pip install -e .
```

to add developpement dependencies:
```bash
$ pip install -e .[dev]
```

and use pre-commit to check your code
```bash
$ pre-commit install
```

## Setup environnement de dev'

```bash
$ tox devenv -e dev .venv`
```

```bash
$ pre-commit install`
```

## Environment variable

- DB_NAME: Name for the database
- CSRF_SECRET: ...
- AUTH_SECRET: ...

# Execution avec Docker

```bash
docker compose up -d
```
serve a local app on port 8000

# Requirement

## **The MerciYanis Music Store**

You have to create a single webpage app for the **MerciYanis Music Store**.

The app must :

- have a welcome page and an album purchase page (including an album list and a product cart) and
- manage thousands of albums.

## **Notes**

You are free to choose your technology stack and the color panel that best suits your application.

## **Code review**

Please make your app available online so we can test it. And send your code on Gitlab on a public project.

You will show us your app in our office (presentation of your technical choices, the potential tests you will have set up, CI/CD, etc.).

**Have fun ! We can’t wait to see your work 🤩**
